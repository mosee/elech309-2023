/*
 * Main file of gpio4.X
 * Author: mosee
 *
 * A LED is connected to RA4 and a push-button is connected to RB14.
 * When the button is pressed, the LED is toogled (its state changes).
 */


#define FCY 3685000     // �cycle frequency. Needed for __delay_ms
#include "libpic30.h"   // Contains __delay_ms definition
#include "xc.h"         // Contains register variable definitions


int main(void) {
    _TRISA4 = 0;    // Configure the LED pin as a digital output pin
    _TRISB14 = 1;   // Configure the button pin as a digital input pin

    // Main (infinite) loop
    while(1) {
        /* When  the button is pressed, the LED is toggled. Then we wait for
         * the release of the button.  This avoid multiple toggling for one
         * button pressing. The delays "filters" button bouncing. */
        if (_RB14) {
            _LATA4 = !_LATA4;
            __delay_ms(100);
            while(_RB14) {
            }
            __delay_ms(100);
        }
    }
    
    return 0;
}


# Exercices

Ce dossier contient des séances d'exercices qui ont pour but de servir de tutoriel à l'utilisation du dsPIC et des ses périphériques les plus courants.
Le fonctionnement général d'un µC est décrit dans [../Hardware/Architecture_uC.pdf](../Hardware/Architecture_uC.pdf)

## [Les Parallel Input/Output (PIO)](/E1-Utilisation_des_PIO.pdf)

C'est le périphérique le plus simple à utiliser.  Il permet au dsPIC d'interagir avec un signal binaire.  
Son fonctionnement est décrit dans [Hardware/Les_PIO.pdf](../Hardware/Les_PIO.pdf)

## [les Timers](/E2-Les_timers.pdf)

Les timers servent à manipuler le temps, pour exécuter une tâche récurrente avec une période précise par exemple.

## [l'UART](/E3-Communication_UART.pdf)

L'UART est un périphérique de communication très utilisé.  il est simple d'utilisation, même si son débit n'est pas très important (1Mbit/s max).  Ici, nous l'utiliserons pour communiquer avec un PC au travers d'un adaptateur USB-UART.

## [ADC et DAC](/E4-ADC_DAC.pdf)

Ce projet montre comment utiliser l'ADC interne du dsPIC et un DAC externe pour acquérir et générer des signaux analogiques.

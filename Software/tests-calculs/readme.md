# tests-calculs

Ce dossier contient les projets qui ont servi à déterminer les temps de calculs des opérations mathématiques utiles à la démodulation :

* [testMult0.X](testMult0.X/testMult0.md) : Ce projet a pour but de comparer le temps de calcul d'une multiplication pour différents types de variables, avec et sans optimisation du compilateur.
